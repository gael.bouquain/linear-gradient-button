import React from "react"
import { Text, TouchableNativeFeedback } from "react-native"
import LinearGradient from "react-native-linear-gradient"

const linearGradientArray = [
	["#003251", "#00415e", "#005069", "#006072", "#006f79"],
	["#4d0076", "#560091", "#5d00ad", "#6106cb", "#6112eb"],
	["#760000", "#942208", "#b13d0c", "#cf5810", "#eb7312"],
	["#006c34", "#0a8849", "#13a460", "#19c278", "#1ce191"],
	["#a65400", "#bd7600", "#cf9b00", "#dac200", "#ddeb05"],
	["#eb1212", "#bc0319", "#8e0218", "#610414", "#370507"],
	["#2f7c63", "#299483", "#1caca6", "#09c3cd", "#11dbf5"],
	["#7d004b", "#9c005e", "#bc0071", "#dd0084", "#ff0098"],
]

export let blue = linearGradientArray[0]
export let blueviolet = linearGradientArray[1]
export let red = linearGradientArray[2]
export let green = linearGradientArray[3]
export let orange = linearGradientArray[4]
export let secondRed = linearGradientArray[5]
export let secondBlue = linearGradientArray[6]
export let pink = linearGradientArray[7]

let randomOfLinearGradient = Math.floor(Math.random() * linearGradientArray.length)
let linearGradient = linearGradientArray[randomOfLinearGradient]



export class RandomGradientButton extends React.Component {
	render() {
		return (
			<TouchableNativeFeedback onPress={this.props.onPress}>
				<LinearGradient
					style={
						!this.props.style
							? { width: 120, height: 35 }
							: this.props.style
					}
					start={{ x: 0, y: 0 }}
					end={{ x: 1, y: 0 }}
					colors={linearGradient}
				>
					<Text>{this.props.children}</Text>
				</LinearGradient>
			</TouchableNativeFeedback>
		)
	}
}

export class GradientButton extends React.Component {
	render() {
		return (
			<TouchableNativeFeedback onPress={this.props.onPress}>
				<LinearGradient
					style={
						!this.props.style
							? { width: 120, height: 35 }
							: this.props.style
					}
					start={{ x: 0, y: 0 }}
					end={{ x: 1, y: 0 }}
					colors={this.props.color || blue}
				>
					<Text>{this.props.children}</Text>
				</LinearGradient>
			</TouchableNativeFeedback>
		)
	}
}
