# linear-gradient-button

These are beautiful react-native components

## Installation
Using Yarn

```
yarn add react-native-linear-gradient
```
Using NPM

```
npm install react-native-linear-gradient
```

## Usage

```javascript
import {
    GradientButton
} react-native-linear-gradient-button';

export export class MyClass extends React.Component {
    render() {
        return(
            <GradientButton color={red}>Hello World !</GradientButton>
        )
    }
}
```
```javascript
import {
    RandomGradientButton
} react-native-linear-gradient-button';

export export class MyClass extends React.Component {
    render() {
        return(
            <RandomGradientButton>Hello World !</RandomGradientButton>
        )
    }
}
```

## License
[MIT](https://choosealicense.com/licenses/mit/)